mod cutter;
mod format;
use format::{Song, SongOption};
use std::{fs::File};
use std::io::{Read, Write};
use soloud::*;
use imagesize;
use rand::seq::SliceRandom;
use show_image::{ImageView, ImageInfo, create_window};

#[derive(Debug, PartialEq)]
enum QuestionResponse {
    PRESKOC,
    ANO,
    NE
}

fn ano_ne_preskoc(question: &str) -> QuestionResponse {
    let mut response = String::new();
    print!("❓ {} (ano/ne)  ", question);
    std::io::stdout().flush().expect("Failed to flush!");
    std::io::stdin().read_line(&mut response).expect("Nepodařilo se načíst odpověď");
    let response = response.trim().to_lowercase();
    let response = response.as_str();
    match  response {
        "ano" => QuestionResponse::ANO,
        "ne" => QuestionResponse::NE,
        "skip" => QuestionResponse::PRESKOC,
        _ => ano_ne_preskoc("Ale už!")
    }

} 

fn odpoved(max: i32) -> i32 {
    let mut user_input = -1;
    while user_input < 0 || user_input >= max {
        let mut buffer = String::new();
        print!("✏️  Zadej odpověď: ");
        std::io::stdout().flush().expect("Failed to flush!");
        std::io::stdin().read_line(&mut buffer).expect("Unable to read");
        user_input = buffer.trim().parse::<i32>().unwrap_or(-1);
    }
    user_input
}

#[show_image::main]
fn main() {
    let mut credits = 5;
    let mut rng = rand::thread_rng();
    let mut file = File::open("/home/ondrej/adamis_mixer/source.yaml").expect("Unable to open");
    let mut content = String::new();
    file.read_to_string(&mut content)
        .expect("Failed to read file");

    let songs: Vec<Song> = serde_yaml::from_str(&content).expect("Failed to parsen()");
    let mut songs_enumerated = songs.iter().enumerate().collect::<Vec<(usize, &Song)>>();
    songs_enumerated.shuffle(&mut rng);
    
    for (round, content) in songs_enumerated.iter().enumerate() {
        let (index, song) = content;
        let mut options = song.options.iter().collect::<Vec<&SongOption>>();
        options.shuffle(&mut rng);
        let image_src = format!("pictures/{}.bmp", index);
        let music_src = format!("songs/{}.short.mp3", index);
        println!("\x1B[2J\x1B[1;1H ");
        println!("⛳ {}. kolo! Máš {} 🪙  do 🎵🗳️🎶\n=========", round + 1, credits);
        println!("📖 Popis\n{}", song.summary);

        let mut response = QuestionResponse::NE;
        while response != QuestionResponse::ANO && response != QuestionResponse::PRESKOC {
            response = ano_ne_preskoc("Máme přečteno?");
        };
        if response == QuestionResponse::PRESKOC {
            continue;
        }
        
        let imgsize = imagesize::size(&image_src).expect("Unable to read image size");
        let mut image_data: Vec<u8> = Vec::new();
        let mut image_file = File::open(image_src).expect("Failed opening");
        
        image_file.read_to_end(&mut image_data).expect("Failed reading to end");
        let image = ImageView::new(ImageInfo::bgr8(imgsize.width as u32, imgsize.height as u32), &image_data);
        let window = create_window("image", Default::default()).expect("failed creating window");
        window.run_function(|mut window| window.set_fullscreen(true));
        window.set_image("image-001", image).expect("Failed setting image");
        
        
        // Hádá
        let response = ano_ne_preskoc("Uhodnuto?");
        if response == QuestionResponse::ANO {
            println!("\n 🍺🍺 My pijeme! 🍺🍺\n");
            ano_ne_preskoc("Dopito?");
            continue;
        }
        if response == QuestionResponse::PRESKOC {
            continue;
        }
        //Jukebox
        if credits > 0 {
            if ano_ne_preskoc("Máme žhavit JUKEBOX?") == QuestionResponse::ANO {
                credits = credits - 1;
                let sl = Soloud::default().unwrap();
                let mut wav = audio::Wav::default();
                let mut sound_data: Vec<u8> = Vec::new();
                let mut sound_file = File::open(music_src).expect("Failed opening");
                sound_file.read_to_end(&mut sound_data).expect("Failed reading to end");
                wav.load_mem(&sound_data).unwrap();
                sl.play(&wav);
                print!("Jukebox spouští: ");
                while sl.voice_count() > 0 {
                    print!(".");
                    std::io::stdout().flush().expect("Failed to flush!");
                    std::thread::sleep(std::time::Duration::from_millis(1000));
                }
                println!(" a dohrál!");
            }
        }
        else {
            println!("🛑  Jukebox tu není pro chudáky! 🛑");
        }
        println!("✅  Možnosti:\n==========");
        for (index, option) in options.iter().enumerate() {
            println!("{}) {}", index, option.name);
        }
        println!("Tak co myslíš?");
        let guess = odpoved(options.len() as i32);
        let value = options[guess as usize];
        
        if value.correct {
            println!("To bylo těsné! Gratulujeme")
        }
        else {
            println!("🍺🍺🍺 Piješ 🍺🍺🍺");
            println!("Pod číslem {} se skrylo: {}", index, options.iter().filter(|i| i.correct).next().unwrap().name);
        }

        ano_ne_preskoc("Můžeme dál?");
    }

    // 
    //
}
