use serde::{Serialize, Deserialize};

fn default_false() -> bool {
    false
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct SongOption {
    #[serde(default)]
    pub name: String,
    #[serde(default = "default_false")]
    pub correct: bool
}




#[derive(Debug, Serialize, Deserialize)]
pub struct Song {
    // Define your YAML structure here
    // For example:
    #[serde(default)]
    pub summary: String,
    #[serde(default)]
    pub options: Vec<SongOption>,
}