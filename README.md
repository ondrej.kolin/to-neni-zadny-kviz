# to-neni-zadny-kviz

Fun drinking project for a bachelor party where songs by a folk group called "Duo Adamis" had to be recongnised by description. More like a proof of concept than a real thing. 

## How to run
1. Install `rust & cargo`
2. Execute `cargo run in the main folder`
3. The data for 

## Example
```
⛳ 1. kolo! Máš 5 🪙  do 🎵🗳️🎶
=========
📖 Popis
Tato píseň oslavuje věk a přátelství. Skupina přátel si uvědomuje, že už nejsou mladí a slaví různé významné životní jubilea. Přátelství je pro ně důležitější než peníze a všechny materiální věci, protože zdraví nelze koupit. Oslava je plná radosti, zábavy a vzpomínek na minulost. Každý si přeje šťastně žít a vychutnat si každý den. Společně si připíjejí a oslavují život. Píseň vyjadřuje vděčnost za přátelství a uvědomění si hodnoty blízkých vztahů i při stárnutí.

❓ Máme přečteno? (ano/ne)  ano
❓ Uhodnuto? (ano/ne)  ne
❓ Máme žhavit JUKEBOX? (ano/ne)  ano
Jukebox spouští: ........... a dohrál!
✅  Možnosti:
==========
0) Padesátka a víc
1) Přání na tisíc
2) Kytička a přání
3) Přátelství nad věkem
Tak co myslíš?
✏️  Zadej odpověď: 1
To bylo těsné! Gratulujeme
❓ Můžeme dál? (ano/ne)  
```
